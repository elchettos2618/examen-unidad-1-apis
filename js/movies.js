
// Realizar la solicitud a la API utilizando fetch

const llamarPelicula = (movieTitle) =>{

    const apiKey = '30063268'; // Reemplaza con tu clave de API
    const plotType = 'full';
    const apiUrl = `https://www.omdbapi.com/?t=${encodeURIComponent(movieTitle)}&plot=${plotType}&apikey=${apiKey}`;

    fetch(apiUrl)
    .then(response => response.json())
    .then(data => {

            mostrarDatos(data, movieTitle)
        })
        .catch(error = () =>{
            
            alert("Error al cargar. No hay coincidencias.");
            console.error('Error en la solicitud:', error)
        }
            );
        

  }

  const mostrarDatos = (data, movieTitle) =>{


        const nombre = document.getElementById('lblNombre')
        const portada = document.getElementById('imgPortada')
        const reseña = document.getElementById('parrReseña')
        const actores = document.getElementById('lblActores')

        const tituloSearch = data.Title.toLowerCase();
        
       
        ///*
        if (tituloSearch === movieTitle.toLowerCase()){
        console.log('Título:', data.Title);
        console.log('Año:', data.Year);
        console.log('Trama:', data.Plot);

        nombre.textContent = data.Title;
        portada.src = data.Poster;
        reseña.textContent = data.Plot;
        actores.textContent = data.Actors;
        // Puedes acceder a más información según tus necesidades
        } else {
            console.error('No hay coincidencias', data.Error);
            alert("No hay coincidencias.")
        } //*/



  }


document.getElementById('btnBuscar').addEventListener('click', () =>{
    const movieTitle = document.getElementById('txtBuscarMovie').value;


    llamarPelicula(movieTitle);


})